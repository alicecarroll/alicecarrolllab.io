window.onload = (_) =>
  document
    .querySelectorAll("input.indeterminate[type=checkbox]")
    .forEach((i) => (i.indeterminate = true));
